//
//  AboutSoftware.swift
//  Lloyds Bank
//
//  Created by Dmitriy Groschovskiy on 29.06.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa
import ParseOSX

class AboutSoftware: NSViewController {
    
    @IBOutlet var bankName : NSTextField!
    @IBOutlet var lastUpdate : NSTextField!
    @IBOutlet var softwareVersion : NSTextField!
    @IBOutlet var currentLanguage : NSTextField!

    override func viewDidLoad() {
        self.getSoftwareInformation()
        super.viewDidLoad()
    }

    // MARK: - Software Information

    func getSoftwareInformation() {
        var request = PFQuery(className:"TLGNSettings")
        request.getObjectInBackgroundWithId("oSb6p047kw") {
            (softwareInformation: PFObject?, error: NSError?) -> Void in
            if error == nil && softwareInformation != nil {
                let bankNameJSON = softwareInformation?["settings_value_one"] as! String
                let bankNameMDK = "\(bankNameJSON)"
                self.bankName.stringValue = bankNameMDK
                let softwareVersionJSON = softwareInformation?["settings_value_two"] as! String
                let softwareVersionMDK = "Software Version: \(softwareVersionJSON)"
                self.softwareVersion.stringValue = softwareVersionMDK
                let currentLanguageJSON = softwareInformation?["settings_value_three"] as! String
                let currentLanguageMDK = "UK Banking System Language: \(currentLanguageJSON)"
                self.currentLanguage.stringValue = currentLanguageMDK
                println(softwareInformation)
            } else {
                println(error)
            }
        }
    }

}
