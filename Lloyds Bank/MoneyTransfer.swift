//
//  MoneyTransfer.swift
//  Lloyds Bank
//
//  Created by Dmitriy Groschovskiy on 29.06.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa

class MoneyTransfer: NSViewController {
    
    @IBOutlet var customerAccountNumber : NSTextField!
    @IBOutlet var recipientAccountNumber : NSTextField!
    @IBOutlet var transferAmountRegistred : NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Transfer Amount Gateway
    
    func transferMoneyInterface() {
        // YOUR_TERMINAL_CODE
    }
    
}
