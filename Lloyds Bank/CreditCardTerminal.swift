//
//  CreditCardTerminal.swift
//  Lloyds Bank
//
//  Created by Dmitriy Groschovskiy on 29.06.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa

class CreditCardTerminal: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.payWithCardReader()
    }
    
    // MARK: - Card Terminal Initialization
    
    func payWithCardReader() {
        print("Code: Code executed")
    }
    
}
