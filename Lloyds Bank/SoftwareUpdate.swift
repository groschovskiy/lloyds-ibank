//
//  SoftwareUpdate.swift
//  Lloyds Bank
//
//  Created by Dmitriy Groschovskiy on 29.06.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa
import ParseOSX
import Foundation

class SoftwareUpdate: NSViewController {

    @IBOutlet var currentVersion : NSTextField!
    @IBOutlet var latestVersion : NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Software Update

    func getSoftwareUpdate() {
        var request = PFQuery(className:"TLGNSettings")
        request.getObjectInBackgroundWithId("3sIcmpffGq") {
            (softwareUpdate: PFObject?, error: NSError?) -> Void in
            if error == nil && softwareUpdate != nil {
                let currentVersionJSON = softwareUpdate?["settings_value_one"] as! String
                let currentVersionMDK = "Current Software Version: \(currentVersionJSON)"
                self.currentVersion.stringValue = currentVersionMDK
                let latestVersionJSON = softwareUpdate?["settings_value_two"] as! String
                let latestVersionMDK = "Latest Software Version: \(latestVersionJSON)"
                self.latestVersion.stringValue = latestVersionMDK
                println(softwareUpdate)
            } else {
                println(error)
            }
        }
    }

    @IBAction func downloadSoftwareUpdate(sender: NSButton) {
        let myURLstring = "https://lloyds.amazon.uk.srv.amazon.com/software/update/Lloyds_App.app"
        let myFilePathString = "~/Downloads"

        if let url = NSURL(string: myURLstring) {
            let imageDataFromURL = NSData(contentsOfURL: url)
        }

        let imageDataFromFile = NSData(contentsOfFile: myFilePathString)
    }

}
