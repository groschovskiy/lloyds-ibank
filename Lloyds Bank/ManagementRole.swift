//
//  ManagementRole.swift
//  Lloyds Bank
//
//  Created by Dmitriy Groschovskiy on 29.06.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa
import ParseOSX

class ManagementRole: NSViewController {
    
    @IBOutlet var fullName : NSTextField!
    @IBOutlet var nationalID : NSTextField!
    @IBOutlet var accountType : NSTextField!

    @IBOutlet var currentCredit : NSTextField!
    @IBOutlet var overdraftCredit : NSTextField!
    @IBOutlet var availableCredit : NSTextField!

    override func viewDidLoad() {
        var timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("receiveCustomerInformation"), userInfo: nil, repeats: true)
        self.receiveCustomerInformation()
        super.viewDidLoad()
    }
    
    // MARK: - Receive Customer Information
    
    func receiveCustomerInformation() {
        var currentUser = PFUser.currentUser()?.objectId
        var customerRequest = PFQuery(className:"_User")
        customerRequest.getObjectInBackgroundWithId(currentUser!) {
            (bankDatabase: PFObject?, error: NSError?) -> Void in
            if error == nil && bankDatabase != nil {
                let bankAccountNumber = bankDatabase?["bankingNumber"] as! String
                let firstNameJSON = bankDatabase?["firstName"] as! String
                let lastNameJSON = bankDatabase?["lastName"] as! String
                let fullNameJSON = "Hey, \(firstNameJSON) \(lastNameJSON)"
                self.fullName.stringValue = fullNameJSON

                var requestCardDB = PFQuery(className:"LLOYBanking")
                requestCardDB.getObjectInBackgroundWithId(bankAccountNumber) {
                    (bankReport: PFObject?, error: NSError?) -> Void in
                    if error == nil && bankReport != nil {
                        let accountTypeJSON = bankReport?["accountType"] as! String
                        self.accountType.stringValue = accountTypeJSON
                        let nationalIdJSON = bankReport?["nationalID"] as! String
                        let nationalNumber = bankReport?["nationalNumber"] as! String
                        let nationalDeclaration = "\(nationalIdJSON), \(nationalNumber)"
                        self.nationalID.stringValue = nationalDeclaration

                        let currentCreditJSON = bankReport?["currentCredit"] as! String
                        var currentCreditIMAX = (currentCreditJSON as NSString).floatValue
                        self.currentCredit.stringValue = "£ \(currentCreditJSON)"
                        let currentOverdraftJSON = bankReport?["currentOverdraft"] as! String
                        var currentOverdraftIMAX = (currentOverdraftJSON as NSString).floatValue
                        let currentOverdraftMDK = "Overdraft limit: £ \(currentOverdraftJSON)"
                        self.overdraftCredit.stringValue = currentOverdraftMDK
                        
                        var availableCreditJSON = currentCreditIMAX + currentOverdraftIMAX
                        self.availableCredit.stringValue = "Money available: £ \(availableCreditJSON)"

                        println(bankReport)
                    } else {
                        println(error)
                    }
                }

                println(bankDatabase)
            } else {
                println(error)
            }
        }
    }
    
    // MARK: - Payment Internet Interface
    
    func paymentInterfaceController() {
        
    }
    
}
