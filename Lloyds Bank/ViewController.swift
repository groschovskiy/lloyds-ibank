//
//  ViewController.swift
//  Lloyds Bank
//
//  Created by Dmitriy Groschovskiy on 29.06.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa
import ParseOSX

class ViewController: NSViewController {

    @IBOutlet var username : NSTextField!
    @IBOutlet var password : NSSecureTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    // MARK: - Authorization Controller

    @IBAction func authWithCredentials(sender: NSButton) {
        PFUser.logInWithUsernameInBackground(username!.stringValue, password:password!.stringValue) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                println("Server: Access Granted")
                let storyboardInitialization = NSStoryboard(name: "Main", bundle: nil)
                let viewControllerInitialization = storyboardInitialization?.instantiateControllerWithIdentifier("ManagementView") as! NSViewController
                self.presentViewControllerAsModalWindow(viewControllerInitialization)
            } else {
                println("Error authorization")
            }
        }
    }

    // MARK: - Help Information Center

    @IBAction func showInternetHelpCenter(sender: NSButton) {
        NSWorkspace.sharedWorkspace().openURL(NSURL(string: "http://www.lloydsbank.com")!)
    }

}

