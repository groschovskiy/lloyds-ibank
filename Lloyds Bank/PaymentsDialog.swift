//
//  PaymentsDialog.swift
//  Lloyds Bank
//
//  Created by Dmitriy Groschovskiy on 03.07.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa
import ParseOSX

class PaymentsDialog: NSViewController {
    
    @IBOutlet var fullName : NSTextField!
    @IBOutlet var passportID : NSTextField!
    @IBOutlet var paymentType : NSTextField!
    @IBOutlet var serialNumber : NSTextField!
    @IBOutlet var paymentAmount : NSTextField!
    @IBOutlet var bankNotification : NSTextField!
    @IBOutlet var phoneConfirmation : NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }

    @IBAction func payWithBankAccount(sender: NSButton) {
        
    }

}
