//
//  ViewController.swift
//  Lloyds Manager
//
//  Created by Dmitriy Groschovskiy on 05.07.15.
//  Copyright (c) 2015 Lloyds Bank PLC. All rights reserved.
//

import Cocoa
import ParseOSX

class ViewController: NSViewController {

    @IBOutlet var serviceStatus : NSTextField!
    @IBOutlet var accountIdentifier : NSTextField!
    @IBOutlet var paymentsAmount : NSTextField!
    @IBOutlet var accountType : NSTextField!
    @IBOutlet var payerFullName : NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.authWithManagementCredentials()
    }

    override var representedObject: AnyObject? {
        didSet {
            println("Object executed!")
        }
    }
    
    func authWithManagementCredentials() {
        PFUser.logInWithUsernameInBackground("cto_london@lloydsbank.com", password:"e5p3rand3") {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                var successAuthorization = "Server: Access Granted"
                self.serviceStatus.stringValue = successAuthorization
            } else {
                var failedAuthorization = "Server: Access Denied"
                self.serviceStatus.stringValue = failedAuthorization
            }
        }
    }
    
    @IBAction func getInformationFromServer(sender: NSButton) {
        var requestAccountIdent = PFQuery(className:"_User")
        requestAccountIdent.getObjectInBackgroundWithId(self.accountIdentifier.stringValue) {
            (accountAdvisor: PFObject?, error: NSError?) -> Void in
            if error == nil && accountAdvisor != nil {
                var currentBankAccountIdentifier = accountAdvisor?["bankingNumber"] as! String
                let firstName = accountAdvisor?["firstName"] as! String
                let lastName = accountAdvisor?["lastName"] as! String
                let fullName = "\(firstName) \(lastName)"
                self.payerFullName.stringValue = fullName
                
                var query = PFQuery(className:"LLOYBanking")
                query.getObjectInBackgroundWithId(currentBankAccountIdentifier) {
                    (bankingAccount: PFObject?, error: NSError?) -> Void in
                    if error == nil && bankingAccount != nil {
                        let accountType = bankingAccount?["accountType"] as! String
                        self.accountType.stringValue = accountType
                        println(bankingAccount)
                    } else {
                        println(error)
                    }
                }
                
                println(accountAdvisor)
            } else {
                println(error)
            }
        }
    }
    
    @IBAction func payWithPapperMoney(sender: NSButton) {
        var requestAccountIdent = PFQuery(className:"_User")
        requestAccountIdent.getObjectInBackgroundWithId(self.accountIdentifier.stringValue) {
            (accountAdvisor: PFObject?, error: NSError?) -> Void in
            if error == nil && accountAdvisor != nil {
                var currentBankAccountIdentifier = accountAdvisor?["bankingNumber"] as! String
                let firstName = accountAdvisor?["firstName"] as! String
                let lastName = accountAdvisor?["lastName"] as! String
                let fullName = "\(firstName) \(lastName)"
                self.payerFullName.stringValue = fullName
                
                var query = PFQuery(className:"LLOYBanking")
                query.getObjectInBackgroundWithId(currentBankAccountIdentifier) {
                    (bankingAccount: PFObject?, error: NSError?) -> Void in
                    if error == nil && bankingAccount != nil {
                        let accountType = bankingAccount?["accountType"] as! String
                        self.accountType.stringValue = accountType
                        let currentCreditDB = bankingAccount?["currentCredit"] as! String
                        let currentCreditFloat = (currentCreditDB as NSString).floatValue
                        let currentCreditValue = self.paymentsAmount.stringValue
                        let currentCreditAmount = (currentCreditValue as NSString).floatValue
                        let currentCreditResult = currentCreditFloat + currentCreditAmount
                        let resultCreditSummary = NSString(format: "%f", currentCreditResult)
                        
                        var query = PFQuery(className:"LLOYBanking")
                        query.getObjectInBackgroundWithId(currentBankAccountIdentifier) {
                            (gameScore: PFObject?, error: NSError?) -> Void in
                            if error != nil {
                                println(error)
                            } else if let gameScore = gameScore {
                                gameScore["currentCredit"] = resultCreditSummary
                                gameScore.save()
                            }
                        }
                        
                        println(bankingAccount)
                    } else {
                        println(error)
                    }
                }
                
                println(accountAdvisor)
            } else {
                println(error)
            }
        }
    }

}

